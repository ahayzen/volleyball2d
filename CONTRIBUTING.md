# Pre-requisites

Install `flatpak`

```
sudo apt install flatpak

flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

# Developing

Use the Godot editor to develop the application.

```
flatpak install flathub org.godotengine.Godot
```

# Flatpak

Building the flatpak requires the `flatpak-builder` and freedesktop SDK, simply run the following commands.

```
sudo apt install flatpak-builder

flatpak install flathub org.freedesktop.Sdk//1.6
```

## Building

Simply run the following command, this will create a build in `.app-dir`

```
./build.sh
```

## Running

After building the app, use the following command to test the app launches.

```
./run.sh
```

## Updating repo

Use the following command to build a repo in `.repo`, then follow the instructions to install the flatpak.

```
./update_repo.sh
```
