
extends Node2D

var defaults = {
	"ui_left": "Left",
	"ui_right": "Right",
	"ui_up": "Up",
	"fullscreen": false,
	"ai_debug": false,
}
var settings = {}

func _ready():
	# Load settings from file
	load_settings()

	# Load global settings
	OS.set_window_fullscreen( get("fullscreen") )
	
	for action in ["ui_left", "ui_right", "ui_up"]:
		var event = InputEventKey.new()
		event.scancode = OS.find_scancode_from_string( get(action) )

		for old_event in InputMap.get_action_list(action):
			if old_event is InputEventKey:
				InputMap.action_erase_event(action, old_event)
		
		InputMap.action_add_event(action, event)

func default_settings():
	save_settings( defaults )

func load_settings():
	var file = File.new();
	if not file.file_exists("user://settings.conf"):
		default_settings()
		return

	file.open("user://settings.conf", File.READ)
	var parsed = parse_json( file.get_as_text() )

	if typeof(parsed) == TYPE_DICTIONARY:
		for key in parsed.keys():
			settings[key] = parsed[key]
	else:
	    print("Expected user://settings.conf to contain a dictionary, is the file corrupt?")

	file.close()

func get(key: String):
	return settings[key]

func save_settings(value: Dictionary):
	var file = File.new()
	file.open("user://settings.conf", File.WRITE)
	file.store_string( to_json( value ) )
	file.close()

	settings = value

func set(key: String, value):
	settings[key] = value
	
	save_settings( settings )