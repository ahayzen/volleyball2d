extends AnimatedSprite

func _ready():
	set_color("#FF0000")

func set_color(color):
	self.set_modulate( Color( color ))

func set_filled(fill):
	set_frame(int(fill))  # 0 - empty, 1 - fill
