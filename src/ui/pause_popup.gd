extends Popup

func _input(event):
	if self.is_visible() and event.is_action_released("ui_cancel"):
		_on_resume_button_pressed()

func _ready():
	get_node("resume_button").connect("pressed", self, "_on_resume_button_pressed")
	get_node("return_button").connect("pressed", self, "_on_return_button_pressed")

	set_process_input(true)

func _on_resume_button_pressed():
	get_tree().set_input_as_handled()  # Handle this event (to stop pause+resume loop)

func _on_return_button_pressed():
	get_node("/root/global").pop_to_top()
	get_tree().paused = false  # ensure that we are not paused