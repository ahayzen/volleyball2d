extends Node2D

onready var label = get_node( "label" )
var time = 0

export var direction = 1
export var start_time = 0

func _ready():
	time = start_time
	get_node("timer").connect("timeout", self, "increment")
	render_text(time)

func get_time():
	return time

func increment():
	time += direction
	render_text(time)

func render_text(t):
	var minutes = floor(t / 60)
	var seconds = floor(t % 60)
	
	if seconds < 10:
		label.set_text(str(minutes, ":0", seconds))
	else:
		label.set_text(str(minutes, ":", seconds))
