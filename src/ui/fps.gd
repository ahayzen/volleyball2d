extends Label

func _ready():
	connect("visibility_changed", self, "_on_visiblity_changed")

func _on_visiblity_changed():
	set_process( is_visible() )

func _process(delta):
	set_text( str( Engine.get_frames_per_second() ) )
