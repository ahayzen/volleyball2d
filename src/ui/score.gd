extends Node2D

export var color = "#FFFFFF"
export var direction = 1  # +ve for LTR -ve for RTL

func _ready():
	# Load colour
	for i in range(1, 6):
		get_node("point_" + str(i)).set_color(color)

func set_point(point, state):
	get_node("point_" + str(point)).set_filled(state)

func set_score(score):
	if direction > 0:
		for i in range(1, 6):
			set_point(i, i <= score)
	else:
		for i in range(1, 6):
			set_point(i, i > (5 - score))