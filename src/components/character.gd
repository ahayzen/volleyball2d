extends RigidBody2D

# 1m = 3GU = 54px
# gravity 20m/s  => 1080px/s

onready var feet = get_node( "feet" )
var MOVE_SPEED = 7 * 54  # 7 m/s
var JUMP_HEIGHT = 25 * 54  # 25 m/s

var pending_teleport_pos = 0

func _ready():
	feet.add_exception(self)

func _integrate_forces(state):
	if typeof(pending_teleport_pos) == TYPE_VECTOR2:
		self.position = pending_teleport_pos
		state.transform.origin = pending_teleport_pos
		pending_teleport_pos = 0

func jump():
	if feet.is_colliding():  # can only jump if touching the ground
		set_axis_velocity(Vector2( 0, -JUMP_HEIGHT ))

func teleport_to_position( pos ):
	pending_teleport_pos = pos

func left(scalar=1.0):
	set_axis_velocity(Vector2( -MOVE_SPEED * scalar, 0 ))

func right(scalar=1.0):
	set_axis_velocity(Vector2( MOVE_SPEED * scalar, 0 ))
