extends RigidBody2D

var pending_teleport_pos = 0

func teleport_to_position( pos ):
	pending_teleport_pos = pos

func _integrate_forces(state):
	if typeof(pending_teleport_pos) == TYPE_VECTOR2:
		self.position = pending_teleport_pos
		state.transform.origin = pending_teleport_pos
		pending_teleport_pos = 0
