extends CollisionPolygon2D

#
# NOTE: THIS CODE IS NOT USED AS THE VALUES ARE HARDCODED!
#

var NODES = 6
var RADIUS = 5 * 18

# FIXME: for now these are hard coded
func semi_circle_as_verticies(radius, nodes):
	var verticies = Vector2Array();
	verticies.append(Vector2(-radius, 0))
	verticies.append(Vector2(radius, 0))
	
	for i in range(1, nodes):
		verticies.append(
			Vector2(
				radius * cos((i * PI) / nodes),
				- radius * sin((i * PI)/ nodes)
			)
		)
		
	return verticies

func _ready():
	pass
