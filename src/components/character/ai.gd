extends "res://components/character.gd"

var CHARACTER_RADIUS = 90
var CONCENTRATION = 0.65  # level of concentration
var GRAVITY = 1080
var JITTER = 18  # 1 GU
var MIN_X = 10  # left boundary (right side of net)
var MAX_X = 630 # right boundary
var RAND_OFFSET

var ball
export var ai_score = 0
export var ai_won_last = false
export var ball_path = ""
export var player_score = 0
export var target_score = 5  # TODO: move into settings
var time = 0

func _ready():
	ball = get_tree().get_root().get_node(ball_path)
	set_physics_process(true)

	RAND_OFFSET = get_position().x / 2

func _physics_process(delta):
	time += delta

	# Limit AI 'think' iterations to one per 100ms
	if time > 0.1:
		iter()
		time = 0

var ai_packet = {
	"actual": 0,
	"angle": 0,
	"highest": 0,
	"landing": 0,
	"target": 0,
}

func get_debug_packet():
	return ai_packet

func set_concentration(value):
	CONCENTRATION = value


#
# Everything below is directly ported from the original volleyball2d written in Qt/Bacon2D
# http://bazaar.launchpad.net/~ahayzen/volleyball2d/edge/view/head:/volleyball2d/components/AIPrediction.qml#L53
#

# Calculate the concentration of the computer based on the scores
func calc_concentration():
	var factor = 5.0

	# Computer only has 'feelings' for firstToX game mode
	# Survival and high score have a fixed concentration of 0.7
	# if settings.gameMode === modeFirstToX:
	# ...
	# else:
	#	 factor = 0.65

	# Computer becomes 'relaxed' as a lead is gained
	# NOTE: in conversion 'enemy' is player
	var diff = ai_score - player_score

	if diff > 0:
		factor -= diff
	else:
		factor += -diff

	# Computer gains concentration towards the end of the match
	factor += max(player_score, ai_score) / 2.0

	# Computer has more focus if the last winner was the enemy
	if not ai_won_last:
		factor += 1.0

	# Convert to value between 0-1
	factor /= 10.0

	if factor < 0.4:
		factor = 0.4
	elif factor > 0.80:
		factor = 0.80

	set_concentration(factor)

func calc_concentration_if_1():
	if CONCENTRATION == 1.0:
		calc_concentration()

func calc_rand_offset():
	RAND_OFFSET = randf() * CHARACTER_RADIUS * 2 * (1 - CONCENTRATION)

func center():
	return self.get_global_position().x

func get_angle(top_point, land_point, point_hit_bound):
	# Get the angle from the two points
	if point_hit_bound < 0:
		top_point.x = 0
	elif point_hit_bound > 0:
		top_point.x = MAX_X

	# tan A = y / x
	# A = atan y / x
	# Note: x and y are flipped from orig formula
	return rad2deg( atan( (land_point.x - top_point.x) / (land_point.y - top_point.y) ) )

func get_angle_as_weight(angle):
	# Get the angle as a weight of a target direction
	# -1 low on left, 0 high, 1 low on right
	return int(angle > 45) - int(angle < -45)

func get_land_point(ball_point, ball_u, top_point, base_y):
	var point = Vector2( top_point.x, base_y )

	# time until the ball touches the top of the character (base_y)
	var s = (top_point.y - base_y)

	# v^2 = u^2 + 2as
	var v = sqrt( abs( 0 + 2 * GRAVITY * s ) )

	# s = 1/2 (u + v) * t
	# t = s / (0.5 * (0 + v))
	var t = s / (0.5 * (0 + v))

	# calculate the X distance in the remaining arc
	point.x += ball_u.x * t

	return point

func get_random_offset(direction):
	# Get random offset depending on direction
	if direction < 0:
		return RAND_OFFSET
	else:
		return -RAND_OFFSET

func get_point_with_bounds(point_hit_bound, point):
	# Check not bounding off a wall
	if point_hit_bound < 0:
		# -200 + (-200 - -201) = -199
		point.x = -MAX_X + (-MAX_X - point.x)  # 0 is center, so flip
	elif point_hit_bound > 0:
		point.x = MAX_X - (point.x - MAX_X)

	return point

func get_top_point(ball_point, ball_u):
	# Get the top point of the curve
	var point = Vector2( ball_point.x, ball_point.y )

	if ball_u.y > 0:  # ball going up
		# calc the diff to the Y top
		# s = (v^2 - u^2) / 2a
		var diff_y = (ball_u.y * ball_u.y ) / (2 * GRAVITY)

		point.x += ball_u.x * (diff_y / ball_u.y)  # assumes X is const and not damping
		point.y += diff_y

	return point

func get_target_weight_within_bounds(target, weight):
	# Get a target position inside the bounds when given a target+weight
	target += weight

	if target < MIN_X:
		target = MIN_X
	elif target > MAX_X:
		target = MAX_X

	return target

func hit_bound(point):
	# Return whether a bound has been hit (-1 left, 0 not hit, +1 right wall)
	return int(point.x > MAX_X) - int(point.x < -MAX_X)

func iter():
	var game_scene_height = 720
	var game_scene_width = 1260

	# Convert to Y=0 is baseline and Vy is positive when going up for easier calculation
	# NOTE: Y is backwards in godot vs bacon2d
	var ball_world_point = ball.get_global_position()
	var ball_point = Vector2( ball_world_point.x, -ball_world_point.y + (game_scene_height / 2) )
	var ball_u = ball.get_linear_velocity()
	ball_u = Vector2( ball_u.x, -ball_u.y )  # Y is backwards


	# Get the top, land points and the angle
	var top_point = get_top_point(ball_point, ball_u)
	var land_point = get_land_point(ball_point, ball_u, top_point, CHARACTER_RADIUS)

	# Include bounds (eg walls) into account
	var land_hit_bound = hit_bound(land_point)
	var pred = get_point_with_bounds(land_hit_bound, land_point)

	# Calculate the landing angle for later
	var angle = get_angle(top_point, pred, land_hit_bound)

	# Convert back to Y=0 in the middle of the screen
	land_point.y = -(land_point.y - (game_scene_height / 2))
	top_point.y = -(top_point.y - (game_scene_height / 2))

	# AI DEBUG
	ai_packet["angle"] = angle
	ai_packet["highest"] = top_point.y
	ai_packet["landing"] = pred.x

	#
	# Decide based on known information actions to perform
	#

	var target = center()
	var weight = 0

	if (abs( ball_world_point.x - center() ) < CHARACTER_RADIUS and
		self.get_global_position().y > ball_world_point.y and
		self.get_global_position().y - CHARACTER_RADIUS - ball_world_point.y < 10 * 18):

		# Ball is within the radius of the character and less than X units above the char
		if ball_world_point.x >= center() - 0.5 * 18:
			# Ball is to the right of the center
			weight = CHARACTER_RADIUS / 2
		elif ball_world_point.x <= center() - 0.5 * 18:
			# Ball is to the left of the center
			weight = -CHARACTER_RADIUS / 2

		self.jump()
	elif pred.x < MIN_X and ball_world_point.x < MIN_X:
		# Ball is out of court (on the other side) and predicted to land there
		# Attempt to return to center-front
		target = MIN_X + (MAX_X - MIN_X) / 2
		weight = -CHARACTER_RADIUS
	elif abs( pred.x - center() ) > 1 * 18:
		# Ball is predicted to land left or right of char (>1 GU)
		target = pred.x
		if pred.x < center():
			weight = -1 * 18
		else:
			weight = 1 * 18

	# Take angle into account
	weight += get_angle_as_weight(angle) * CHARACTER_RADIUS

	# AI DEBUG
	ai_packet["target"] = target + weight

	# Randomly alter the weight offset (<= character.width)
	weight += get_random_offset(angle)

	# Ensure that target is within the bounds of player
	target = get_target_weight_within_bounds(target, weight)

	# AI DEBUG
	ai_packet["actual"] = target

	move_to(target)

	"""
	# Perfect AI!
	move_to(ball.get_global_pos().x + RAND_OFFSET)

	if abs(ball.get_global_pos().y - self.get_global_pos().y) - self.get_pos().height < 36:
		if abs(ball.get_global_pos().x - self.get_global_pos().x) < 100:
			self.jump()
	"""

func move_to(target):
	var center = center()

	if abs(center - target) > JITTER:
		if center > target:
			self.left(scalar_for_diff(center - target))
		else:
			self.right(scalar_for_diff(target - center))

func scalar_for_diff(diff):
	var max_threshold = 180  # 10 GU
	var scalar_limit = 2  # Character.scalar_limit

	if diff > max_threshold:
		return scalar_limit
	else:
		return (diff / max_threshold) * scalar_limit
