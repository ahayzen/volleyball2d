extends "res://components/character.gd"

func _ready():
	set_process(true)

func _process(delta):
	# Note we don't use _input as we want to listen for pressed
	# without waiting for the button to repeat
	
	# Process keyboard input
	if Input.is_action_pressed("ui_left"):
		self.left()

	if Input.is_action_pressed("ui_right"):
		self.right()

	if Input.is_action_pressed("ui_up"):
		self.jump()

	# Process joystick input?

	# Process motion input
	# Accel is -10 -> 10 with -10 being left down
	var accel = Input.get_accelerometer()
	var MOTION_DEAD_ZONE = 1.0
	
	#var accel_x = accel.x
	#print(accel_x, Input.get_gyroscope())

	if accel.x != 0.0 and abs(accel.x) > MOTION_DEAD_ZONE:
		if accel.x < 0:
			self.right( get_scalar( accel.x ) )
		else:
			self.left( get_scalar( accel.x ) )

func get_scalar(amount):
	# Make a scale from 0 -> 2
	# Android accel is from 0 -> 10
	# Sensitivity can be 0.5 - 1
	var MOTION_SENSITIVITY = 1
	var scalar_limit = 2  # Character.scalar_limit

	var scalar = ((abs(amount) / 10) * 2) * MOTION_SENSITIVITY

	if scalar > scalar_limit:
		scalar = scalar_limit

	return scalar
