extends "res://scenes/world.gd"

onready var ai = $ai

# TODO: split into child component
onready var selected = $container/left/selected
var selected_index = 0

var options = [
	"container/left/buttons/first_to_five",
	"container/left/buttons/survival",
	"container/left/buttons/highest_score",
]

var options_scenes = [
	"res://scenes/game/first_to_five.tscn",
	"res://scenes/game/survival.tscn",
	"res://scenes/game/highest_score.tscn",
]

var description = [
	"First to score five points",
	"Highest score within two minutes",
	"Highest score without losing a point",
]

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_node("/root/global").pop()

func _ready():
	$container/right/start.connect("pressed", self, "start")
	$container/left/settings.connect("pressed", self, "settings")
	$container/left/exit.connect("pressed", self, "exit")
	
	$container/right/start.grab_focus()

	# Load label saying buttons for user
	$container/right/label.set_text(
		"Press " + action_to_string("ui_up") +
		", " + action_to_string("ui_left") +
		", " + action_to_string("ui_right") +
		" to move your character\n" +
		"Press " + action_to_string("ui_fullscreen") + " to toggle fullscreen mode"
	)
	
	for i in range(len(options)):
		get_node(options[i]).connect("pressed", self, "set_selected", [i]);

	reset()

	connect( "ballHitGround", self, "_on_ball_hit_ground" )
	connect( "ballHitObject", self, "_on_ball_hit_object" )

	# TODO: this should be read from settings
	set_selected( selected_index )

	set_process_input(true)

func action_to_string(action):
	for event in InputMap.get_action_list(action):
		if event is InputEventKey:
			return OS.get_scancode_string( event.scancode )

	return "None"

func exit():
	get_node("/root/global").pop()

func _on_ball_hit_ground( direction ):
	# AI missed the ball so reset
	reset()

func _on_ball_hit_object( objects ):
	# when ball hits things recalc AI's random 'human' offset
	ai.calc_rand_offset()

func settings():
	get_node("/root/global").push("res://scenes/settings.tscn")

func set_selected(index):
	var x = selected.get_global_position().x;
	var y = get_node(options[index]).get_global_position().y \
		+ (get_node(options[index]).rect_size.y / 2) \
		- (selected.rect_size.y / 2)
	selected.set_global_position( Vector2( x, y ) );

	$container/left/description.text = description[index]

	selected_index = index

func start():
	get_node("/root/global").push(options_scenes[selected_index])

func reset():
	reset_ball( 1 )
	
	# Reset player positions
	ai.teleport_to_position( Vector2( 315, 315 ) )
	ai.set_linear_velocity( Vector2( 0.0, 0.0 ) )

	# AI to 'perfect' and start the ball on the AI side
	ai.set_concentration(1.0)
	ai.calc_rand_offset()
