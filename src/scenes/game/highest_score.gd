extends "res://scenes/game.gd"

onready var clock = get_node("clock")

var player_score = 0

func _ready():
	connect( "aiHitBall", self, "_on_ai_hit_ball" )
	connect( "roundWon", self, "_on_round_won" )

	render_ui()

	set_process(true)

func _process(delta):
	if clock.get_time() <= 0:
		pause("You scored " + str(player_score), PAUSE_MODE.END_GAME)

# AI concentration level is fixed to 0.65 for highest score mode
func _on_ai_hit_ball(ai):
	ai.set_concentration(0.65)

func _on_round_won(winner):
	if winner != ai:
		player_score += 1

	render_ui()

	if winner == player:
		pause("You won this round", PAUSE_MODE.END_ROUND)
	else:
		pause("You lost this round", PAUSE_MODE.END_ROUND)

	reset(winner)

func render_ui():
	get_node("player_score").set_text(str(player_score))
