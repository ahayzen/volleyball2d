
extends "res://scenes/game.gd"

var ai_score = 0
var player_score = 0

func _ready():
	connect( "aiHitBall", self, "_on_ai_hit_ball" )
	connect( "roundWon", self, "_on_round_won" )

	render_ui()

# Tell the AI to recomputer concentration if not already set
# When it is first hit by the ball (first shot should always be perfect)
func _on_ai_hit_ball(ai):
	ai.calc_concentration_if_1()

func _on_round_won(winner):
	if winner == ai:
		ai_score += 1
	else:
		player_score += 1

	ai.ai_won_last = winner == ai
	ai.ai_score = ai_score
	ai.player_score = player_score

	render_ui()

	if ai_score == 5:
		pause("You lost the match!", PAUSE_MODE.END_GAME)
	elif player_score == 5:
		pause("You won the match!", PAUSE_MODE.END_GAME)
	else:
		if winner == player:
			pause("You won this round", PAUSE_MODE.END_ROUND)
		else:
			pause("You lost this round", PAUSE_MODE.END_ROUND)

		reset(winner)

func render_ui():
	get_node("player_score").set_score(player_score)
	get_node("ai_score").set_score(ai_score)
