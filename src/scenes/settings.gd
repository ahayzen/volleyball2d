extends CenterContainer

onready var ai_debug_button = get_node("grid/ai_debug_button")
onready var fullscreen_button = get_node("grid/fullscreen_button")
onready var jump_button = get_node("grid/jump_button")
onready var left_button = get_node("grid/left_button")
onready var right_button = get_node("grid/right_button")
var waiting_action = ""

func _input(event):
	if event is InputEventKey:
		# Handle the event and don't wait any longer
		get_tree().set_input_as_handled()
		set_process_input( false )

		# If the event is not cancel then change the action
		if not event.is_action("ui_cancel") and waiting_action != "":
			update_action(waiting_action, event)

		# Ensure buttons are up to date
		redraw_buttons()

func _on_ai_debug_toggled(state):
	get_node("/root/store").set("ai_debug", state)

func _on_fullscreen_toggled(state):
	OS.set_window_fullscreen( state )
	get_node("/root/store").set("fullscreen", state)

func _on_return_pressed():
	get_node("/root/global").pop()

func _ready():
	ai_debug_button.connect("toggled", self, "_on_ai_debug_toggled")
	fullscreen_button.connect("toggled", self, "_on_fullscreen_toggled")
	jump_button.connect("pressed", self, "_wait_for_input", [jump_button, "ui_up"])
	left_button.connect("pressed", self, "_wait_for_input", [left_button, "ui_left"])
	right_button.connect("pressed", self, "_wait_for_input", [right_button, "ui_right"])
	
	get_node("grid/return_button").connect("pressed", self, "_on_return_pressed")

	set_process_input( true )

	redraw_buttons()

func _wait_for_input(button, action):
	button.set_text("-")
	waiting_action = action  # ui_up
	set_process_input( true )

func action_to_string(action):
	for event in InputMap.get_action_list(action):
		if event is InputEventKey:
			return OS.get_scancode_string( event.scancode )

	return "None"

func redraw_buttons():
	fullscreen_button.set_text( action_to_string("ui_fullscreen") )
	jump_button.set_text( action_to_string("ui_up") )
	left_button.set_text( action_to_string("ui_left") )
	right_button.set_text( action_to_string("ui_right") )
	
	ai_debug_button.set_pressed( get_node("/root/store").get("ai_debug") )
	fullscreen_button.set_pressed( get_node("/root/store").get("fullscreen") )

func update_action(action, event):
	for old_event in InputMap.get_action_list(action):
		if old_event is InputEventKey:
			InputMap.action_erase_event(action, old_event)

	InputMap.action_add_event(action, event)

	get_node("/root/store").set(action, OS.get_scancode_string( event.scancode ) )