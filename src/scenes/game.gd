extends "res://scenes/world.gd"

onready var ai = get_node( "ai" )
var ai_debug
var game_pause_mode = PAUSE_MODE.NONE
onready var player = get_node("player")

export var ball_start_player = "player"

var ai_debug_packet = {
	"actual": 0,
	"angle": 0,
	"highest": 0,
	"landing": 0,
	"target": 0,
}
var ubuntu_font = DynamicFont.new()

enum PAUSE_MODE {
	NONE,
	USER,
	END_ROUND,
	END_GAME,
}

signal aiHitBall(ai)
signal roundWon(winner)


func _draw():
	# TODO: only run when ai debug is enabled
	if ai_debug_packet["highest"] == 0:
		return

	# Draw the high Y point
	draw_string( ubuntu_font, Vector2( 0, ai_debug_packet["highest"] ), "Highest", Color( "#FF0000" ) )
	draw_line( Vector2( -1000, ai_debug_packet["highest"] ), Vector2( 1000, ai_debug_packet["highest"] ), Color( "#FF0000" ) )

	# Draw the landing X point
	draw_string( ubuntu_font, Vector2( ai_debug_packet["landing"], -100 ), "Landing", Color( "#0000FF" ) )
	draw_line( Vector2( ai_debug_packet["landing"], -1000 ), Vector2( ai_debug_packet["landing"], 1000 ), Color( "#0000FF" ) )

	# Perfect X point to go
	draw_string( ubuntu_font, Vector2( ai_debug_packet["target"], 0 ), "Target", Color( "#00FF00" ) )
	draw_line( Vector2( ai_debug_packet["target"], -1000 ), Vector2( ai_debug_packet["target"], 1000), Color( "#00FF00" ) )

	# Randomized X point
	draw_string( ubuntu_font, Vector2( ai_debug_packet["actual"], 100 ), "Actual", Color( "#000099" ) )
	draw_line( Vector2( ai_debug_packet["actual"], -1000), Vector2( ai_debug_packet["actual"], 1000), Color( "#000000" ) )

	# Draw the angle

	# tan A = y / x
	# y = x * tan A
	# Note: x and y are flipped from orig formula
	var angle_x = 100 * tan( deg2rad( ai_debug_packet["angle"] ) )
	draw_line( Vector2( ai_debug_packet["landing"], 200), Vector2( ai_debug_packet["landing"] - angle_x, 300), Color( "#555555" ) )

func _input(event):
	if event.is_action_released("ui_cancel"):
		get_tree().set_input_as_handled()  # Handle this event (to stop pause+resume loop)
		pause("Paused", PAUSE_MODE.USER)

func _ready():
	ai_debug = get_node("/root/store").get("ai_debug")

	var ubuntu_font_data = DynamicFontData.new()
	ubuntu_font_data.font_path = "res://assets/fonts/ubuntu/Ubuntu-M.ttf"
	ubuntu_font.font_data = ubuntu_font_data
	ubuntu_font.size = 20
	ubuntu_font.use_filter = true
	ubuntu_font.use_mipmaps = true

	get_node("pause_popup/resume_button").connect("pressed", self, "resume")
	get_node("pause_button").connect("pressed", self, "pause", ["Paused", PAUSE_MODE.USER])

	reset(get_node(ball_start_player))  # start with the player

	connect( "ballHitGround", self, "_on_ball_hit_ground" )
	connect( "ballHitObject", self, "_on_ball_hit_object" )

	set_process(true)
	set_process_input(true)

	if ai_debug:
		get_node("fps").show()

func _process(delta):
	if ai_debug:
		ai_debug_packet = ai.get_debug_packet()
		update()

func _on_ball_hit_ground( direction ):
	if direction == -1:
		emit_signal( "roundWon", ai )
	elif direction == 1:
		emit_signal( "roundWon", player )

func _on_ball_hit_object( objects ):
	# Tell the AI to recomputer concentration if not already set
	# When it is first hit by the ball (first shot should always be perfect)
	if ai in objects:
		emit_signal( "aiHitBall", ai )

	# when ball hits things recalc AI's random 'human' offset
	ai.calc_rand_offset()

func pause(text, mode):
	game_pause_mode = mode

	# Toggle return to menu button
	if game_pause_mode == PAUSE_MODE.USER:
		get_node("pause_popup/return_button").show()
	else:
		get_node("pause_popup/return_button").hide()

	# Disable ball monitoring otherwise when we resume
	# we get a hit event - this sortof acts like flushing it
	ball.set_contact_monitor(false)

	get_tree().set_pause( true )
	get_node("pause_popup/winner").set_text(text)
	get_node("pause_popup").show()
	get_node("pause_popup/resume_button").grab_focus()

func resume():
	get_node("pause_popup").hide()
	get_tree().set_pause( false )

	# If we have ended the match then pop when resuming
	if game_pause_mode == PAUSE_MODE.END_GAME:
		get_node("/root/global").pop_to_top()
	else:
		# Reenable ball monitoring so that it can hit things
		ball.set_contact_monitor(true)

func reset(winner):
	reset_ball( 1 if winner == ai else -1 )

	# Reset player positions
	ai.teleport_to_position( Vector2( 315, 315 ) )
	ai.set_linear_velocity( Vector2( 0.0, 0.0 ) )
	player.teleport_to_position( Vector2( -315, 315 ) )
	player.set_linear_velocity( Vector2( 0.0, 0.0 ) )

	# Set AI concentration to perfect until ball touches
	ai.set_concentration(1.0)
	ai.calc_rand_offset()
