extends Node2D

onready var ball = get_node("ball")
onready var ball_hint = get_node("ball_hint")
onready var ground_left = get_node("ground_left")
onready var ground_right = get_node("ground_right")

signal ballHitGround(side)  # -1 for left, 1 for right
signal ballHitObject(objs)  # unknown object hit

func _ready():
	ball.set_max_contacts_reported(3)  # left/right, character, ground
	ball.set_contact_monitor(true)

	set_physics_process(true)

func _physics_process(delta):
	if ball.contact_monitor:
		var bodies = ball.get_colliding_bodies()

		if bodies.size() > 0:
			if ground_left in bodies:
				emit_signal( "ballHitGround", -1 )
			elif ground_right in bodies:
				emit_signal( "ballHitGround", 1 )
			else:
				emit_signal( "ballHitObject", bodies )

	# Update ball position
	var ball_pos = ball.get_global_position()

	if ball_pos.y < -351:  # only show hint if ball is offscreen
		ball_hint.set_global_position( Vector2( ball_pos.x, -351 ) )
		ball_hint.show()
	else:
		ball_hint.hide()

func reset_ball(direction):
	# Ensure the ball isn't spinning or moving still
	ball.set_angular_velocity( 0.0 )
	ball.set_linear_velocity( Vector2( 0.0, 0.0 ) )
	ball.teleport_to_position( Vector2( 315 * direction, 0 ) )

