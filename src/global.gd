
extends Node2D

func _input(event: InputEvent):
	if event.is_action_pressed("ui_fullscreen"):
		OS.set_window_fullscreen(not OS.is_window_fullscreen())
		get_node("/root/store").set("fullscreen", OS.is_window_fullscreen())

func _ready():
	# When paused keep processing
	set_pause_mode( PAUSE_MODE_PROCESS )

	# TODO: only handle unhandled?
	# set_process_unhandled_input(true)
	set_process_input(true)

	stack.append(get_tree().get_current_scene().get_filename())


#
# Scene stack management
#

var stack = []

func _switch_scene():
	get_tree().change_scene(stack.back())

func pop():
	# If there is one or zero in the stack then quit
	if stack.size() < 2:
		get_tree().quit()
	else:
		stack.pop_back()
		_switch_scene()

func pop_to_top():
	while stack.size() > 1:
		stack.pop_back()

	_switch_scene()

func push(scene: String):
	stack.append(scene)
	_switch_scene()