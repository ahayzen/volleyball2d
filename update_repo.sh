#!/bin/bash

flatpak-builder --repo=.repo --ccache --force-clean .app-dir com.ahayzen.volleyball2d.json

echo "
# Please add the repository and install the flatpak
flatpak --user remote-add --no-gpg-verify volleyball2d-local-nightly $PWD/.repo
flatpak --user install volleyball2d-local-nightly com.ahayzen.volleyball2d

# If already installed simply run
flatpak update

# Run the app
flatpak run com.ahayzen.volleyball2d
"
